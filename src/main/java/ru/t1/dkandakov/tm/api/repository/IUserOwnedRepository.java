package ru.t1.dkandakov.tm.api.repository;

import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void removeAll(String userId);

    boolean existsById(String UserId, String Id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String useId, String Id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M removeOneById(String userId, String id);

    M removeOneByIndex(String userId, Integer index);

    M add(final String UserId, M model);

    M removeOne(final String userId, M model);

}