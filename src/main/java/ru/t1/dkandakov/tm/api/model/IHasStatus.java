package ru.t1.dkandakov.tm.api.model;

import ru.t1.dkandakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}