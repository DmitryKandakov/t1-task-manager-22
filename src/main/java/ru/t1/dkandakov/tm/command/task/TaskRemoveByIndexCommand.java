package ru.t1.dkandakov.tm.command.task;

import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().removeOneByIndex(userId, index);
    }

    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

}