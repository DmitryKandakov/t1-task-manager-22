package ru.t1.dkandakov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Dmitry Kandakov");
        System.out.println("email: dkandakov@t1-consulting.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show info about programmer.";
    }

}