package ru.t1.dkandakov.tm.command.project;

import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectShowProjectsCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final ProjectSort projectSort = ProjectSort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, projectSort.getComparator());
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show project list.";
    }

}