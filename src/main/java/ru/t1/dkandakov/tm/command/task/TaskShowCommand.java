package ru.t1.dkandakov.tm.command.task;

import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.TaskSort;
import ru.t1.dkandakov.tm.model.Task;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort taskSort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, taskSort.getComparator());
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show task list.";
    }

}
