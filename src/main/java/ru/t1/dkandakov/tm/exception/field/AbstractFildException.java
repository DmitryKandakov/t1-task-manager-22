package ru.t1.dkandakov.tm.exception.field;

import ru.t1.dkandakov.tm.exception.AbstractException;

public class AbstractFildException extends AbstractException {

    public AbstractFildException() {
    }

    public AbstractFildException(final String message) {
        super(message);
    }

    public AbstractFildException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFildException(final Throwable cause) {
        super(cause);
    }

    public AbstractFildException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}