package ru.t1.dkandakov.tm.enumerated;

import ru.t1.dkandakov.tm.comparator.CreatedComparator;
import ru.t1.dkandakov.tm.comparator.NameComparator;
import ru.t1.dkandakov.tm.comparator.StatusComparator;
import ru.t1.dkandakov.tm.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    private final String Name;

    private final Comparator<Project> comparator;

    public static ProjectSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final ProjectSort projectSort : values()) {
            if (projectSort.name().equals(value)) return projectSort;
        }
        return null;
    }

    ProjectSort(String displayName, Comparator<Project> comparator) {
        this.Name = displayName;
        this.comparator = comparator;
    }

    public String getName() {
        return Name;
    }

    public Comparator<Project> getComparator() {
        return comparator;
    }

}