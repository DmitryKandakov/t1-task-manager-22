package ru.t1.dkandakov.tm.enumerated;

import ru.t1.dkandakov.tm.comparator.CreatedComparator;
import ru.t1.dkandakov.tm.comparator.NameComparator;
import ru.t1.dkandakov.tm.comparator.StatusComparator;
import ru.t1.dkandakov.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    private final String name;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort taskSort : values()) {
            if (taskSort.name().equals(value)) return taskSort;
        }
        return null;
    }

    TaskSort(String displayName, Comparator<Task> comparator) {
        this.name = displayName;
        this.comparator = comparator;
    }

    public String getName() {
        return name;
    }

    @SuppressWarnings("rawtypes")
    public Comparator<Task> getComparator() {
        return comparator;
    }

}