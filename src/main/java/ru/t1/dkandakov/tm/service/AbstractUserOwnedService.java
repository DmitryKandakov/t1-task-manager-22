package ru.t1.dkandakov.tm.service;

import ru.t1.dkandakov.tm.api.repository.IUserOwnedRepository;
import ru.t1.dkandakov.tm.api.service.IUserOwnedService;
import ru.t1.dkandakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.dkandakov.tm.exception.field.IdEmptyException;
import ru.t1.dkandakov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkandakov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final M model = repository.findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M removeOne(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(userId, model);
    }

    @Override
    public M removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = findOneById(userId, id);
        return removeOne(userId, model);
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        final M model = findOneByIndex(userId, index);
        return removeOne(userId, model);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @Override
    public int getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

}